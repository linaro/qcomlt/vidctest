/*--------------------------------------------------------------------------
Copyright (c) 2010-2011, Code Aurora Forum. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Code Aurora nor
      the names of its contributors may be used to endorse or promote
      products derived from this software without specific prior written
      permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NON-INFRINGEMENT ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------*/
#include "decoder_driver_test.h"

#define DEBUG_PRINT printf
/************************************************************************/
/*        #DEFINES                          */
/************************************************************************/

#define VOP_START_CODE 0x000001B6
#define SHORT_HEADER_START_CODE 0x00008000
#define H264_START_CODE         0x00000001

/************************************************************************/
/*        STATIC VARIABLES                          */
/************************************************************************/
#define MEM_HEAP_ID ION_CP_MM_HEAP_ID

static int Codec_type;
static int total_frames = 0;
static unsigned int header_code = 0,file_type=0;
static pthread_mutex_t read_lock;
int reconfig =0;

static unsigned int read_frame ( unsigned char *dataptr,unsigned int length,
                                 FILE * inputBufferFile
                                );
static int Read_Buffer_From_DAT_File( unsigned char *dataptr, unsigned int length,
                                      FILE * inputBufferFile
                                     );
static unsigned int read_frame_from_container(unsigned char *dataptr,unsigned int length,struct demux *demux_ptr);

static unsigned clp2(unsigned x)
{
  x = x - 1;
  x = x | (x >> 1);
  x = x | (x >> 2);
  x = x | (x >> 4);
  x = x | (x >> 8);
  x = x | (x >>16);
  return x + 1;
}
static void* video_thread (void *);
static void* async_thread (void *);
int m_vdec_ion_devicefd;
static AVFormatContext *open_file(const char *filename)
{
  AVFormatContext *afc= avformat_alloc_context();
  int err = avformat_open_input(&afc,filename, NULL,NULL);
  printf("\navformat_open_input completed:\n");
  if (!err)
    err = avformat_find_stream_info(afc,NULL);
  if (err < 0) {
    printf("\n%s: lavf error %d\n", filename, err);
    exit(1);
  }
  //dump_format(afc, 0, filename, 0);
  printf("\nopen_file Exit:\n");
  return afc;
}/* open_file */

static AVStream *find_stream(AVFormatContext *afc1)
{
  AVStream *st = NULL;
  unsigned int i;
  printf("\nfind-stream Entered:\n");
  for (i = 0; i < afc1->nb_streams; i++) {
    if (afc1->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO && !st)
      st = afc1->streams[i];
    else
      afc1->streams[i]->discard = AVDISCARD_ALL;
  }
  printf("\nfind-stream Exit:\n");
  return st;
}/* find_stream*/

void usage(char *name)
{

  printf("\n\nWelcome!! Test of A-Family Qualcomm video decoder.\n\n");
  printf("\t-h, --help: Print this help and exit.\n");

  printf("\n\nfile_type 1 for container file, \n\t 2 for bit stream file \n");

  printf("\n\nFor video container file Usage: %s <file_type> <H264_INFILE>\n", name);
  printf("\nEx: %s 1 xyz.3gp\n", name);

  printf("\n\nFor mp4 bit sream file Usage: %s <file_type> <bitstream_file> <decoder_format> <frame_width> <frame_height>\n", name);
  printf("\nEx: %s 2 mp4-input-bitstream.m4v m4v 800 480\n", name);

  printf("\n\nFor H264 bit sream file Usage: %s <file_type> <bitstream_file> <decoder_format> <frame_width> <frame_height>\n", name);
  printf("\nEx: %s 2 h264-input-bitstream.264 264 800 480\n", name);

  printf("\n\nOut put will be generated in output.yuv in the same folder\n");
}/* usage*/

/* Control starts from here */
int main (int argc, char **argv)
{
  struct video_decoder_context *decoder_context = NULL;
  char *file_name = NULL,*decoder_format = NULL;
  FILE *file_ptr = NULL;
  int temp1 =0,temp2 =0;
  int error = 1;
  unsigned int i = 0,type=0;
  reconfig = 0;

  if(argc == 1)
  {
    printf("\nInvalid args, plz refer help\n");
    usage(argv[0]);
    exit(0);
  }
  for (i = 1; i < argc; i++) {
    if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help")) {
      usage(argv[0]);
      exit(0);
    }
  }

  memset(&h264_mv_buff,0,sizeof(struct h264_mv_buffer));
  /* Allocate memory for the decode context */
  decoder_context = (struct video_decoder_context *) \
                   calloc (sizeof (struct video_decoder_context),1);
  if (decoder_context == NULL)
  {
    return -1;
  }
  decoder_context->outputBufferFile = NULL;
  decoder_context->inputBufferFile = NULL;
  decoder_context->video_driver_fd = -1;

  file_type = atoi(argv [1]);
  if(file_type < 1 ||file_type > 2  )
  {
     printf("\nfile_type invalid arg\n");
     usage(argv[0]);
     exit(0);
  }

  if(file_type == 1)
  {
    if(argc != 3)
    {
      printf("\nInvalid args, plz refer help\n");
      usage(argv[0]);
      exit(0);
    }
    else
    {
      /* Open input file and store in decode context */
      file_name = argv [2];

      /* header parsing */
      // Register all formats and codecs
      av_register_all();
      avcodec_register_all();
      printf("\nregistered all:");
      if(file_name != NULL)
      {
        AVFormatContext *afc = open_file(file_name);
	AVStream *st = find_stream(afc);
	AVCodecContext *cc = st->codec;
	AVBitStreamFilterContext *bsf = NULL;
        int size,width=0,height=0;
        AVPacket pk = {};

	printf("\nopen_stream Entered coded_id : %d\n",cc->codec_id);

	if (cc->extradata && cc->extradata_size > 0 && cc->extradata[0] == 1) {
	  printf("initializing bitstream filter");
	  bsf = av_bitstream_filter_init("h264_mp4toannexb");
	  if (!bsf) {
	    printf("could not open '%s': failed to initialize bitstream filter", file_name);
	    return -1;
	  }
	}
	decoder_context->video_resoultion.frame_width = cc->width;
	decoder_context->video_resoultion.frame_height = cc->height;

	decoder_context->demux_data = (struct demux*)malloc(sizeof(struct demux));
	decoder_context->demux_data->afc = afc;
	decoder_context->demux_data->cc = cc;
	decoder_context->demux_data->st = st;
	decoder_context->demux_data->bsf = bsf;

        if(cc->codec_id == AV_CODEC_ID_H264)
        {
          DEBUG_PRINT("\n codec format VDEC_CODECTYPE_H264 ");
          decoder_context->decoder_format = VDEC_CODECTYPE_H264;
          Codec_type = VDEC_CODECTYPE_H264;
        }
        else if(cc->codec_id == AV_CODEC_ID_MPEG4)
	    {
          DEBUG_PRINT("\n codec format MPEG4 ");
          decoder_context->decoder_format = VDEC_CODECTYPE_MPEG4;
          Codec_type = VDEC_CODECTYPE_MPEG4;
	    }
        else
        {
          DEBUG_PRINT("\n codec format not suported ");
          return -1;
        }
        size = cc->width * cc->height;
        printf("\nopen_stream cc->codec_id:%d\n",cc->codec_id);
        printf("\nopen_stream width:%d , height = %d\n",cc->width,cc->height);
      }
    }
  }/*if(file_type == 1) */
  else if(file_type == 2)
  {
    if(argc != 6)
    {
      printf("\nInvalid args, plz refer help\n");
      usage(argv[0]);
      exit(0);
    }
    else
    {
      /* Open input file and store in decode context */
      file_name = argv [2];
      file_ptr = fopen (file_name,"rb");
      if (file_ptr == NULL)
      {
        DEBUG_PRINT("\n Input File is not located ");
        return -1;
      }
      decoder_context->inputBufferFile = file_ptr;

      decoder_format = argv[3];
      if(!strcmp(decoder_format,"m4v"))
      {
        decoder_context->decoder_format = VDEC_CODECTYPE_MPEG4;
        DEBUG_PRINT("\n codec format VDEC_CODECTYPE_MPEG4");
        Codec_type = VDEC_CODECTYPE_MPEG4;
      }
      else if(!strcmp(decoder_format,"264"))
      {
        DEBUG_PRINT("\n codec format VDEC_CODECTYPE_H264 ");
        decoder_context->decoder_format = VDEC_CODECTYPE_H264;
        Codec_type = VDEC_CODECTYPE_H264;
      }
      else
      {
        printf("\nInvalid args, plz refer help\n");
        usage(argv[0]);
        exit(0);
      }
      decoder_context->video_resoultion.frame_width = atoi(argv [4]);
      decoder_context->video_resoultion.frame_height = atoi(argv [5]);
    }
  }

  /*Open output file and store in decode context*/
  file_ptr = fopen ("output.yuv","wb");
  if (file_ptr == NULL)
  {
    DEBUG_PRINT("\n File can't be created");
    free (decoder_context);
    return -1;
  }
  decoder_context->outputBufferFile = file_ptr;

  /* Open ion drive for memory allocations */
  m_vdec_ion_devicefd = open ("/dev/ion", O_RDONLY);

  if (m_vdec_ion_devicefd < 0) {
    DEBUG_PRINT("\nERROR: ION Device open() Failed -------- \n");
    return -1;
  }

  /* All the inputs are all right now, start the decode */
  if ( error != -1 && (init_decoder (decoder_context) == -1 ))
  {
    DEBUG_PRINT("\n Init decoder fails ");
    error = -1;
  }
  DEBUG_PRINT("\n Decoder Initialized successfull");

  /*Allocate input and output buffers*/
  if (error != -1 && (ion_allocate_buffer(VDEC_BUFFER_TYPE_INPUT,
                      decoder_context)== -1))
  {
    DEBUG_PRINT("\n Error in input Buffer allocation");
    error = -1;
  }
  if (error != -1 && (ion_allocate_buffer(VDEC_BUFFER_TYPE_OUTPUT,
                      decoder_context)== -1))
  {
    DEBUG_PRINT("\n Error in output Buffer allocation");
    error = -1;
  }

  if (error != -1 && (start_decoding (decoder_context) == -1))
  {
    DEBUG_PRINT("\n Error in start decoding call");
    error = -1;
  }

  if (error != -1 && (stop_decoding (decoder_context) == -1))
  {
    DEBUG_PRINT("\n Error in stop decoding call");
    error = -1;
  }
   
  DEBUG_PRINT("\n  waiting for threads to join \n");
  pthread_join( decoder_context->asyncthread_id, NULL);
  pthread_join( decoder_context->videothread_id, NULL);
  DEBUG_PRINT("\n Threads are finished with proce\n");

  if(decoder_context->demux_data->cc->codec_id == AV_CODEC_ID_H264)
  {
    DEBUG_PRINT("\n >>>>>>> codec format VDEC_CODECTYPE_H264 ");
  }
  else if(decoder_context->demux_data->cc->codec_id == AV_CODEC_ID_MPEG4)
  {
    DEBUG_PRINT("\n >>>>>>> codec format MPEG4 ");
  }
  printf("\n >>>>>>> open_stream width:%d , height = %d\n",decoder_context->demux_data->cc->width,decoder_context->demux_data->cc->height);

  if (error != -1)
  {
    DEBUG_PRINT ("\n >>>>>>> Total Number of frames decoded %d\n",total_frames);
    /* Decoding is completed,now we need to de-initialize the decoder */
    DEBUG_PRINT("\n De-init the decoder Enter");
    if ((deinit_decoder (decoder_context) == -1))
    {
      error = -1;
    }

    (void)free_buffer (VDEC_BUFFER_TYPE_INPUT,decoder_context);
    (void)free_buffer (VDEC_BUFFER_TYPE_OUTPUT,decoder_context);
    DEBUG_PRINT("\n input & output buffers free completed \n");
    if (decoder_context->inputBufferFile != NULL)
    {
      fclose (decoder_context->inputBufferFile);
    }
    if (decoder_context->outputBufferFile != NULL)
    {
      fclose (decoder_context->outputBufferFile);
    }
    DEBUG_PRINT(" input and output files are closed video_decoder is going to close ");
    close (decoder_context->video_driver_fd);
    DEBUG_PRINT("\n free the decoder_context and return \n");
    free(decoder_context);
  }
  return error;
}

int init_decoder ( struct video_decoder_context *init_decode )
{
  struct vdec_ioctl_msg ioctl_msg = {NULL,NULL};
  struct video_queue_context *queue_ptr = NULL;
//#ifdef MAX_RES_720P
//  enum vdec_output_fromat output_format = VDEC_YUV_FORMAT_NV12;
//#endif
//#ifdef MAX_RES_1080P
  enum vdec_output_fromat output_format  = VDEC_YUV_FORMAT_TILE_4x2;
//#endif

  pthread_mutexattr_t init_values;

  DEBUG_PRINT("\n Before calling the open and the output_format is : 0x%x ", output_format);

  init_decode->video_driver_fd = open ("/dev/msm_vidc_dec", \
                     O_RDWR | O_NONBLOCK);



  if (init_decode->video_driver_fd < 0)
  {
    DEBUG_PRINT("\n /dev/msm_vidc_dev Open failed");
    return -1;
  }


  /*Initialize Decoder with codec type and resolution*/
  ioctl_msg.in = &init_decode->decoder_format;
  ioctl_msg.out = NULL;

  if (ioctl (init_decode->video_driver_fd,VDEC_IOCTL_SET_CODEC,
         (void*)&ioctl_msg) < 0)
  {
    DEBUG_PRINT("\n Set codec type failed");
    return -1;
  }

  /*Set the output format*/
  ioctl_msg.in = &output_format;
  ioctl_msg.out = NULL;

  if (ioctl (init_decode->video_driver_fd,VDEC_IOCTL_SET_OUTPUT_FORMAT,
         (void*)&ioctl_msg) < 0)
  {
    DEBUG_PRINT("\n Set output format failed");
    return -1;
  }

  ioctl_msg.in = &init_decode->video_resoultion;
  ioctl_msg.out = NULL;

  if (ioctl (init_decode->video_driver_fd,VDEC_IOCTL_SET_PICRES,
         (void*)&ioctl_msg) < 0)
  {
    DEBUG_PRINT("\n Set Resolution failed");
    return -1;
  }
  DEBUG_PRINT("\n After Set Resolution");

  DEBUG_PRINT("\n Query Input bufffer requirements");
  /*Get the Buffer requirements for input and output ports*/
  init_decode->input_buffer.buffer_type = VDEC_BUFFER_TYPE_INPUT;
  ioctl_msg.in = NULL;
  ioctl_msg.out = &init_decode->input_buffer;

  if (ioctl (init_decode->video_driver_fd,VDEC_IOCTL_GET_BUFFER_REQ,
         (void*)&ioctl_msg) < 0)
  {
    DEBUG_PRINT("\n Requesting for input buffer requirements failed");
    return -1;
  }

  DEBUG_PRINT("\n input Size=%d min count =%d actual count = %d, maxcount = %d,alignment = %d,buf_poolid= %d,meta_buffer_size=%d",\
              init_decode->input_buffer.buffer_size,\
              init_decode->input_buffer.mincount,\
              init_decode->input_buffer.actualcount,\
              init_decode->input_buffer.maxcount,\
              init_decode->input_buffer.alignment,\
              init_decode->input_buffer.buf_poolid,\
              init_decode->input_buffer.meta_buffer_size);


  init_decode->input_buffer.buffer_type = VDEC_BUFFER_TYPE_INPUT;
  ioctl_msg.in = &init_decode->input_buffer;
  ioctl_msg.out = NULL;
  init_decode->input_buffer.actualcount = init_decode->input_buffer.mincount + 2;

  if (ioctl (init_decode->video_driver_fd,VDEC_IOCTL_SET_BUFFER_REQ,
         (void*)&ioctl_msg) < 0)
  {
    DEBUG_PRINT("\n Set Buffer Requirements Failed");
    return -1;
  }


  DEBUG_PRINT("\n Query output bufffer requirements");
  init_decode->output_buffer.buffer_type = VDEC_BUFFER_TYPE_OUTPUT;
  ioctl_msg.in = NULL;
  ioctl_msg.out = &init_decode->output_buffer;

  if (ioctl (init_decode->video_driver_fd,VDEC_IOCTL_GET_BUFFER_REQ,
         (void*)&ioctl_msg) < 0)
  {
    DEBUG_PRINT("\n Requesting for output buffer requirements failed");
    return -1;
  }

  DEBUG_PRINT("\n output_buffer Size=%d min count =%d actual count = %d, maxcount = %d,alignment = %d,buf_poolid= %d,meta_buffer_size=%d",\
              init_decode->output_buffer.buffer_size,\
              init_decode->output_buffer.mincount,\
              init_decode->output_buffer.actualcount,\
              init_decode->output_buffer.maxcount,\
              init_decode->output_buffer.alignment,\
              init_decode->output_buffer.buf_poolid,\
              init_decode->output_buffer.meta_buffer_size);

/*    init_decode->disable_dmx = 0;
      ioctl_msg.in = NULL;
      ioctl_msg.out = &init_decode->disable_dmx;
      if (ioctl(init_decode->video_driver_fd, VDEC_IOCTL_GET_DISABLE_DMX_SUPPORT, &ioctl_msg))
      {
        DEBUG_PRINT("Error VDEC_IOCTL_GET_DISABLE_DMX_SUPPORT");
        return -1;
      }
*/
  /*Create Queue related data structures*/
  queue_ptr = &init_decode->queue_context;
  queue_ptr->commandq_size = 50;
  queue_ptr->dataq_size = 50;

  sem_init(&queue_ptr->sem_message,0, 0);
  sem_init(&init_decode->sem_synchronize,0, 0);

  pthread_mutexattr_init (&init_values);
  pthread_mutex_init (&queue_ptr->mutex,&init_values);
  pthread_mutex_init (&read_lock,&init_values);
  DEBUG_PRINT("\n create Queues");
  queue_ptr->ptr_cmdq = (struct video_msgq*) \
                        calloc (sizeof (struct video_msgq),
                  queue_ptr->commandq_size);
  queue_ptr->ptr_dataq = (struct video_msgq*) \
              calloc (sizeof (struct video_msgq),
                  queue_ptr->dataq_size
                  );

  if ( queue_ptr->ptr_cmdq == NULL ||
     queue_ptr->ptr_dataq == NULL)
  {
    return -1;
  }
  DEBUG_PRINT("\n create Threads");
  /*Create two threads*/
  if ( (pthread_create (&init_decode->videothread_id,NULL,video_thread,
            init_decode) < 0) ||
         (pthread_create (&init_decode->asyncthread_id,NULL,async_thread,
            init_decode) < 0))
  {
    return -1;
  }
  return 1;
}



int free_buffer ( enum vdec_buffer buffer_dir,
                  struct video_decoder_context *decode_context
                 )
{
  unsigned int buffercount = 0,i=0;
  struct vdec_bufferpayload *ptemp = NULL;
  struct vdec_ioctl_msg ioctl_msg = {NULL,NULL};
  struct vdec_setbuffer_cmd setbuffers;

  if (decode_context == NULL)
  {
    return -1;
  }

  if (buffer_dir == VDEC_BUFFER_TYPE_INPUT && decode_context->ptr_inputbuffer)
  {
    buffercount = decode_context->input_buffer.actualcount;
    ptemp = decode_context->ptr_inputbuffer;

    for (i=0;i<buffercount;i++)
    {
      if (ptemp [i].pmem_fd != -1)
      {
        munmap ( ptemp [i].bufferaddr,ptemp [i].mmaped_size);
        ptemp [i].bufferaddr = NULL;
        close (ptemp [i].pmem_fd);
      }
    }
    free (decode_context->ptr_inputbuffer);
    decode_context->ptr_inputbuffer = NULL;
  }
  else if ( buffer_dir == VDEC_BUFFER_TYPE_OUTPUT )
  {
    buffercount = decode_context->output_buffer.actualcount;
    ptemp = decode_context->ptr_outputbuffer;

    /* free mv buffers */
    if(h264_mv_buff.pmem_fd > 0)
    {
      if(ioctl(decode_context->video_driver_fd, VDEC_IOCTL_FREE_H264_MV_BUFFER,NULL) < 0)
        DEBUG_PRINT("VDEC_IOCTL_FREE_H264_MV_BUFFER failed");
      munmap(h264_mv_buff.buffer, h264_mv_buff.size);

      if (close(decode_context->h264_mv.fd_ion_data.fd)) {
       DEBUG_PRINT("\n ION: close(%d) failed",
          decode_context->h264_mv.fd_ion_data.fd);
     }
     if(ioctl(decode_context->h264_mv.ion_device_fd,ION_IOC_FREE,
             &decode_context->h264_mv.ion_alloc_data.handle)) {
       DEBUG_PRINT("\n ION: free failed, dev_fd = %d, handle = 0x%p",
          decode_context->h264_mv.ion_device_fd, decode_context->h264_mv.ion_alloc_data.handle);
     }
     decode_context->h264_mv.ion_device_fd = -1;
     decode_context->h264_mv.ion_alloc_data.handle = NULL;
     decode_context->h264_mv.fd_ion_data.fd = -1;

//      DEBUG_PRINT(" Cleaning H264_MV buffer of size %d",h264_mv_buff.size);
      h264_mv_buff.pmem_fd = -1;
      h264_mv_buff.offset = 0;
      h264_mv_buff.size = 0;
      h264_mv_buff.count = 0;
      h264_mv_buff.buffer = NULL;
    }
    /* free output buffers */
    ioctl_msg.in = &decode_context->output_buffer;
    ioctl_msg.out = NULL;

    for (i=0;i<buffercount;i++)
    {
      if (ptemp [i].pmem_fd != -1)
      {
        setbuffers.buffer_type = VDEC_BUFFER_TYPE_OUTPUT;
        memcpy (&setbuffers.buffer,&decode_context->ptr_outputbuffer[i],sizeof (struct vdec_bufferpayload));
        ioctl_msg.in  = &setbuffers;
        ioctl_msg.out = NULL;

        if (ioctl (decode_context->video_driver_fd, VDEC_IOCTL_FREE_BUFFER,
	          &ioctl_msg) < 0)
        {
          DEBUG_PRINT("\n Release output buffer failed in VCD");
        }
        munmap ( ptemp [i].bufferaddr,ptemp [i].mmaped_size);
        ptemp [i].bufferaddr = NULL;
        close (ptemp [i].pmem_fd);
      }
    }
    if (decode_context->ptr_respbuffer)
    {
      free (decode_context->ptr_respbuffer);
      decode_context->ptr_respbuffer = NULL;
    }
    if(decode_context->ptr_outputbuffer)
    {
      free (decode_context->ptr_outputbuffer);
      decode_context->ptr_outputbuffer = NULL;
    }
  }
  return 1;
}

int ion_allocate_buffer ( enum vdec_buffer buffer_dir,
                      struct video_decoder_context *decode_context
                    )
{
  struct vdec_setbuffer_cmd setbuffers;
  struct vdec_bufferpayload **ptemp = NULL;
  struct vdec_ioctl_msg ioctl_msg = {NULL,NULL};
  unsigned int buffercount = 0,i=0,alignedsize=0;
  unsigned int buffersize = 0;
  struct ion_allocation_data *alloc_data;
  int rc = -1;
  int fd = -1;
  int pmem_fd = -1;
  unsigned char *buf_addr = NULL;
  int ii;

  if ( decode_context == NULL)
  {
    DEBUG_PRINT ("\nallocate_buffer: context is NULL");
    return -1;
  }

  if ( buffer_dir == VDEC_BUFFER_TYPE_INPUT )
  {
    /*Check if buffers are allocated*/
    if (decode_context->ptr_inputbuffer != NULL)
    {
      DEBUG_PRINT ("\nallocate_buffer: decode_context->ptr_inputbuffer is set");
      return -1;
    }
    DEBUG_PRINT("\n Allocate i/p buffer Header: Cnt(%d) Sz(%d) \n",
                  decode_context->input_buffer.actualcount,decode_context->input_buffer.buffer_size);

    decode_context->ptr_inputbuffer = (struct vdec_bufferpayload *) \
          calloc ((sizeof (struct vdec_bufferpayload)),decode_context->input_buffer.actualcount);
    if (decode_context->ptr_inputbuffer == NULL)
    {
      DEBUG_PRINT("\n ptr_inputbuffer allocated FAIL ---\n");
      return -1;
    }
    decode_context->ip_buf_ion_info = (struct vdec_ion *) \
             calloc ((sizeof (struct vdec_ion)),decode_context->input_buffer.actualcount);

    if (decode_context->ip_buf_ion_info == NULL)
    {
      DEBUG_PRINT("\n ip_buf_ion_info allocated FAIL ---\n");
      return -1;
    }
    for (i=0; i < decode_context->input_buffer.actualcount; i++)
    {
      decode_context->ptr_inputbuffer [i].pmem_fd = -1;
      decode_context->ip_buf_ion_info[i].ion_device_fd = -1;
    }

    for(i=0;i < decode_context->input_buffer.actualcount; i++)
    {
      struct ion_allocation_data *alloc_data = &decode_context->ip_buf_ion_info[i].ion_alloc_data;
      struct ion_fd_data *fd_data            = &decode_context->ip_buf_ion_info[i].fd_ion_data;
      fd = m_vdec_ion_devicefd;
      alloc_data->len = decode_context->input_buffer.buffer_size;
      alloc_data->flags = 0x1;
      alloc_data->align = 2048;
      alloc_data->heap_mask = 0x2000000;

      rc = ioctl(fd,ION_IOC_ALLOC,alloc_data);
      if (rc || !alloc_data->handle) {
		DEBUG_PRINT("\n ION ALLOC failed, fd = %d, rc = %d, handle = 0x%p, "
                       "errno ", fd, rc, alloc_data->handle);
		alloc_data->handle = NULL;
		return -1;
      }
      DEBUG_PRINT("ION memory allocated successfully \n");
      fd_data->handle = alloc_data->handle;
      rc = ioctl(fd,ION_IOC_MAP,fd_data);
      if (rc) {
        DEBUG_PRINT("\n ION MAP failed, fd = %d, handle = 0x%p, errno =",
			fd, fd_data->handle);
        fd_data->fd = -1;
        return -1;
      }

      decode_context->ip_buf_ion_info[i].ion_device_fd = fd;
      pmem_fd = decode_context->ip_buf_ion_info[i].fd_ion_data.fd;
      buf_addr = (unsigned char *)mmap(NULL,
          decode_context->input_buffer.buffer_size,
          PROT_READ|PROT_WRITE, MAP_SHARED, pmem_fd, 0);
      if (buf_addr == MAP_FAILED)
      {
        pmem_fd = -1;
        DEBUG_PRINT("\n Map Failed to allocate input buffer");
        return -1;
      }
      decode_context->ptr_inputbuffer [i].bufferaddr = buf_addr;
      decode_context->ptr_inputbuffer [i].pmem_fd = pmem_fd;
      decode_context->ptr_inputbuffer [i].buffer_len = decode_context->input_buffer.buffer_size;
      decode_context->ptr_inputbuffer [i].mmaped_size = decode_context->input_buffer.buffer_size;
      decode_context->ptr_inputbuffer [i].offset = 0;

      setbuffers.buffer_type = VDEC_BUFFER_TYPE_INPUT;
      memcpy (&setbuffers.buffer,&decode_context->ptr_inputbuffer [i],sizeof(struct vdec_bufferpayload));
      ioctl_msg.in  = &setbuffers;
      ioctl_msg.out = NULL;

      if (ioctl (decode_context->video_driver_fd,VDEC_IOCTL_SET_BUFFER,
	         &ioctl_msg) < 0)
      {
        DEBUG_PRINT("\n Set Buffers Failed");
        return -1;
      }
      DEBUG_PRINT("\ninput  ion_alloc: buf_addr = %p, len = %d, size = %d, \n",\
		buf_addr,decode_context->input_buffer.buffer_size,decode_context->input_buffer.buffer_size);
    }/* for loop*/
    DEBUG_PRINT ("\nInput buffers ion_allocate_buffer: Success");
  } /* VDEC_BUFFER_TYPE_INPUT  */
  if( buffer_dir == VDEC_BUFFER_TYPE_OUTPUT )
  {
    DEBUG_PRINT("\n\nAllocate o/p buffer Header: Cnt(%d) Sz(%d)",
      decode_context->output_buffer.actualcount,
      decode_context->output_buffer.buffer_size);

    decode_context-> ptr_outputbuffer = (struct vdec_bufferpayload *)\
      calloc (sizeof(struct vdec_bufferpayload),
      decode_context->output_buffer.actualcount);

    decode_context->ptr_respbuffer = (struct vdec_output_frameinfo  *)\
      calloc (sizeof (struct vdec_output_frameinfo),
      decode_context->output_buffer.actualcount);

    decode_context->op_buf_ion_info = (struct vdec_ion *)\
      calloc (sizeof(struct vdec_ion),
      decode_context->output_buffer.actualcount);

    for(i=0; i < decode_context->output_buffer.actualcount ; i++)
    {
      decode_context->ptr_outputbuffer[i].pmem_fd = -1;
      decode_context->op_buf_ion_info[i].ion_device_fd =-1;
      /*Create a mapping between buffers*/
      decode_context->ptr_respbuffer[i].client_data = (void *)\
                                            &decode_context->ptr_outputbuffer[i];
    }
    for (i=0; i< decode_context->output_buffer.actualcount; i++)
    {
      struct ion_allocation_data *alloc_data = &decode_context->op_buf_ion_info[i].ion_alloc_data;
      struct ion_fd_data *fd_data            = &decode_context->op_buf_ion_info[i].fd_ion_data;
      fd = m_vdec_ion_devicefd;
      alloc_data->len = decode_context->output_buffer.buffer_size;
      alloc_data->flags = 0x1;
      alloc_data->align = 8192;
      alloc_data->heap_mask = 0x2000000;
      rc = ioctl(fd,ION_IOC_ALLOC,alloc_data);

      if (rc || !alloc_data->handle) {
        DEBUG_PRINT("\n ION ALLOC failed, fd = %d, rc = %d, handle = 0x%p, "
                       "errno ", fd, rc, alloc_data->handle);
        alloc_data->handle = NULL;
        return -1;
      }
      fd_data->handle = alloc_data->handle;
      rc = ioctl(fd,ION_IOC_MAP,fd_data);
	if (rc) {
		DEBUG_PRINT("\n ION MAP failed, fd = %d, handle = 0x%p, errno",
				fd, fd_data->handle);
        fd_data->fd = -1;
	return -1;
      }

      decode_context->op_buf_ion_info[i].ion_device_fd = fd;
      pmem_fd = decode_context->op_buf_ion_info[i].fd_ion_data.fd;
      buf_addr = (unsigned char *)mmap(NULL,
          decode_context->output_buffer.buffer_size,
          PROT_READ|PROT_WRITE, MAP_SHARED, pmem_fd, 0);
      if (buf_addr == MAP_FAILED)
      {
        pmem_fd = -1;
        DEBUG_PRINT("\n Map Failed to allocate input buffer");
        return -1;
      }
      decode_context->ptr_outputbuffer [i].bufferaddr = buf_addr;
      decode_context->ptr_outputbuffer [i].pmem_fd = pmem_fd;
      decode_context->ptr_outputbuffer [i].buffer_len = decode_context->output_buffer.buffer_size;
      decode_context->ptr_outputbuffer [i].mmaped_size = decode_context->output_buffer.buffer_size;
      decode_context->ptr_outputbuffer [i].offset = 0;

      setbuffers.buffer_type = VDEC_BUFFER_TYPE_OUTPUT;
      memcpy (&setbuffers.buffer,&decode_context->ptr_outputbuffer [i],sizeof(struct vdec_bufferpayload));
      ioctl_msg.in  = &setbuffers;
      ioctl_msg.out = NULL;

      if (ioctl (decode_context->video_driver_fd,VDEC_IOCTL_SET_BUFFER,
	         &ioctl_msg) < 0)
      {
        DEBUG_PRINT("\n Set Buffers Failed");
        return -1;
      }
      DEBUG_PRINT("\noutput ion_alloc: buf_addr = %p, len = %d, size = %d, \n",\
		buf_addr,decode_context->output_buffer.buffer_size,decode_context->output_buffer.buffer_size);

    } /* for loop */
    DEBUG_PRINT ("\nprasanth --------- output ion_allocate_buffer: Success");
  } /* VDEC_BUFFER_TYPE_OUTPUT */
  return 1;
}


int start_decoding (struct video_decoder_context *decode_context)
{
  struct vdec_ioctl_msg ioctl_msg = {NULL,NULL};
  struct vdec_input_frameinfo frameinfo;
  struct vdec_fillbuffer_cmd fillbuffer;
  unsigned int i = 0;
  unsigned int data_len =0;

  memset ((unsigned char*)&frameinfo,0,sizeof (struct vdec_input_frameinfo));
  memset ((unsigned char*)&fillbuffer,0,sizeof (struct vdec_fillbuffer_cmd));

  if (decode_context == NULL)
  {
    return -1;
  }

  if (ioctl (decode_context->video_driver_fd,VDEC_IOCTL_CMD_START,
         NULL) < 0)
  {
    DEBUG_PRINT("\n Start failed");
    return -1;
  }

  DEBUG_PRINT("\n Start Issued successfully waiting for Start Done");
  /*Wait for Start command response*/
    sem_wait (&decode_context->sem_synchronize);

  /*Push output Buffers*/
  i = 0;
  while (i < decode_context->output_buffer.mincount)
  {
    fillbuffer.buffer.buffer_len =
                               decode_context->ptr_outputbuffer [i].buffer_len;
    fillbuffer.buffer.bufferaddr =
                               decode_context->ptr_outputbuffer [i].bufferaddr;
    fillbuffer.buffer.offset =
                               decode_context->ptr_outputbuffer [i].offset;
    fillbuffer.buffer.pmem_fd =
                               decode_context->ptr_outputbuffer [i].pmem_fd;
    fillbuffer.client_data = (void *)&decode_context->ptr_respbuffer[i];
    DEBUG_PRINT ("\n Client Data on output = %p and bufferaddr = %p",fillbuffer.client_data,decode_context->ptr_outputbuffer [i].bufferaddr);
    ioctl_msg.in = &fillbuffer;
    ioctl_msg.out = NULL;

    if (ioctl (decode_context->video_driver_fd,
           VDEC_IOCTL_FILL_OUTPUT_BUFFER,&ioctl_msg) < 0)
    {
      DEBUG_PRINT("\n Decoder frame failed");
      return -1;
    }
    i++;
  }


  /*push input buffers*/
  i = 0;
  while (i < decode_context->input_buffer.mincount)
  {
  /* read data from container file */
  if(file_type == 1)
  {
    /*For MPEG, we need to send the header to video driver */
    if(decode_context->decoder_format = VDEC_CODECTYPE_MPEG4)
    {
      memcpy(decode_context->ptr_inputbuffer [i].bufferaddr,decode_context->demux_data->cc->extradata,decode_context->demux_data->cc->extradata_size);
      data_len = decode_context->demux_data->cc->extradata_size;
    }
    else if(decode_context->decoder_format = VDEC_CODECTYPE_H264)
    {
    data_len = read_frame_from_container(decode_context->ptr_inputbuffer [i].bufferaddr,\
                       decode_context->ptr_inputbuffer [i].buffer_len,decode_context->demux_data);
    }

  }
  else if(file_type == 2) /* read data from bitstream */
  {
    data_len = read_frame ( decode_context->ptr_inputbuffer [i].bufferaddr,
                       decode_context->ptr_inputbuffer [i].buffer_len,
             decode_context->inputBufferFile);
  }


    if (data_len == 0)
    {
      DEBUG_PRINT("\n Length is zero error");
      return -1;
    }
    DEBUG_PRINT("\n Read  Frame from File szie = %u",data_len);
    frameinfo.bufferaddr =
    decode_context->ptr_inputbuffer [i].bufferaddr;
    frameinfo.offset = 0;
    frameinfo.pmem_fd = decode_context->ptr_inputbuffer [i].pmem_fd;
    frameinfo.pmem_offset = decode_context->ptr_inputbuffer [i].offset;
    frameinfo.datalen = data_len;
    frameinfo.client_data = (struct vdec_bufferpayload *)&decode_context->ptr_inputbuffer [i];
    /*TODO: Time stamp needs to be updated*/
    ioctl_msg.in = &frameinfo;
    ioctl_msg.out = NULL;
    DEBUG_PRINT ("\n input frame Client Data  address = %p",frameinfo.client_data);
    if (ioctl (decode_context->video_driver_fd,VDEC_IOCTL_DECODE_FRAME,
         &ioctl_msg) < 0)
    {
      DEBUG_PRINT("\n Decoder frame failed");
      return -1;
    }
    total_frames++;
    i++;
  }
  DEBUG_PRINT ("\n Wait for EOS");
  /*Wait for EOS or Error condition*/
  sem_wait (&decode_context->sem_synchronize);
  DEBUG_PRINT ("\n Reached EOS");

  return 1;
}

int stop_decoding  (struct video_decoder_context *decode_context)
{
  struct vdec_ioctl_msg ioctl_msg = {NULL,NULL};
  enum vdec_bufferflush flush_dir = VDEC_FLUSH_TYPE_INPUT;

  if (decode_context == NULL)
  {
    return -1;
  }

  ioctl_msg.in = &flush_dir;
  ioctl_msg.out = NULL;

  if (ioctl(decode_context->video_driver_fd,VDEC_IOCTL_CMD_FLUSH,
         &ioctl_msg) < 0)
  {
    DEBUG_PRINT("\n Flush input failed");
  }
  else
  {
       sem_wait (&decode_context->sem_synchronize);
  }

  flush_dir = VDEC_FLUSH_TYPE_OUTPUT;
  ioctl_msg.in = &flush_dir;
  ioctl_msg.out = NULL;

  if (ioctl(decode_context->video_driver_fd,VDEC_IOCTL_CMD_FLUSH,
         &ioctl_msg) < 0)
  {
    DEBUG_PRINT("\n Flush output failed");
  }
  else
  {
     sem_wait (&decode_context->sem_synchronize);
  }

  DEBUG_PRINT("\n Stop VDEC_IOCTL_CMD_STOP");
  if (ioctl(decode_context->video_driver_fd,VDEC_IOCTL_CMD_STOP,
         NULL) < 0)
  {
    DEBUG_PRINT("\n Stop failed");
  }
  else
  {
     sem_wait (&decode_context->sem_synchronize);
  }
  return 1;
}

int deinit_decoder (struct video_decoder_context *init_decode)
{
  if (init_decode == NULL)
  {
    return -1;
  }

  if (init_decode->queue_context.ptr_cmdq)
  {
    free (init_decode->queue_context.ptr_cmdq);
    init_decode->queue_context.ptr_cmdq = NULL;
  }
  if (init_decode->queue_context.ptr_dataq)
  {
    free (init_decode->queue_context.ptr_dataq);
    init_decode->queue_context.ptr_dataq = NULL;
  }
  sem_destroy (&init_decode->queue_context.sem_message);
  sem_destroy (&init_decode->sem_synchronize);

  pthread_mutex_destroy(&init_decode->queue_context.mutex);
  pthread_mutex_destroy (&read_lock);
  DEBUG_PRINT("\n all threads are closed\n");
  return 1;
}
int vdec_alloc_h264_mv(void *context)
{
  int pmem_fd = -1,fd=-1,rc=-1;
  int width, height, size, alignment;
  void *buf_addr = NULL;
  struct vdec_ioctl_msg ioctl_msg;
 // struct pmem_allocation allocation;
  struct vdec_h264_mv h264_mv;
  struct vdec_mv_buff_size mv_buff_size;
  struct video_decoder_context *decode_context = NULL;

  decode_context = (struct video_decoder_context *) context;

  decode_context->video_resoultion.stride = decode_context->video_resoultion.frame_width;
  decode_context->video_resoultion.scan_lines = decode_context->video_resoultion.frame_height;

  mv_buff_size.width = decode_context->video_resoultion.stride;
  mv_buff_size.height = decode_context->video_resoultion.scan_lines>>2;

  ioctl_msg.in = NULL;
  ioctl_msg.out = (void*)&mv_buff_size;

  if (ioctl (decode_context->video_driver_fd,VDEC_IOCTL_GET_MV_BUFFER_SIZE, (void*)&ioctl_msg) < 0)
  {
    DEBUG_PRINT("\n GET_MV_BUFFER_SIZE Failed for width: %d, Height %d" ,
      mv_buff_size.width, mv_buff_size.height);
    return -1;
  }

  DEBUG_PRINT("\nGET_MV_BUFFER_SIZE returned: Size: %d and alignment: %d",
                    mv_buff_size.size, mv_buff_size.alignment);

  size = mv_buff_size.size * decode_context->output_buffer.actualcount;
  alignment = mv_buff_size.alignment;

  DEBUG_PRINT("\nEntered vdec_alloc_h264_mv act_width: %d, act_height: %d, size: %d, alignment %d",
                   decode_context->video_resoultion.frame_width, decode_context->video_resoultion.frame_height,size,alignment);

  struct ion_allocation_data *alloc_data = &decode_context->h264_mv.ion_alloc_data;
  struct ion_fd_data *fd_data            = &decode_context->h264_mv.fd_ion_data;
  fd = m_vdec_ion_devicefd;
  alloc_data->len = size;
  alloc_data->flags = 0x1;
  alloc_data->align = 8196;
  alloc_data->heap_mask = 0x2000000;
  rc = ioctl(fd,ION_IOC_ALLOC,alloc_data);
  if (rc || !alloc_data->handle) {
    DEBUG_PRINT("\n ION ALLOC failed, fd = %d, rc = %d, handle = 0x%p, "
                       "errno ", fd, rc, alloc_data->handle);
    alloc_data->handle = NULL;
    return -1;
  }
  fd_data->handle = alloc_data->handle;
  rc = ioctl(fd,ION_IOC_MAP,fd_data);
  if (rc) {
    DEBUG_PRINT("\n ION MAP failed, fd = %d, handle = 0x%p, errno =",
			fd, fd_data->handle);
    fd_data->fd = -1;
    return -1;
  }
  decode_context->h264_mv.ion_device_fd = fd;
  pmem_fd = decode_context->h264_mv.fd_ion_data.fd;

  buf_addr = mmap(NULL, size,
                   PROT_READ | PROT_WRITE,
                   MAP_SHARED, pmem_fd, 0);

  if (buf_addr == (void*) MAP_FAILED)
  {
    pmem_fd = -1;
    DEBUG_PRINT("Error returned in allocating recon buffers buf_addr: %p\n",buf_addr);
    return -1;
  }
  DEBUG_PRINT("Allocated virt:%p, FD: %d of size %d count: %d", buf_addr,
                   pmem_fd, size, decode_context->output_buffer.actualcount);

  h264_mv.size = size;
  h264_mv.count = decode_context->output_buffer.actualcount;
  h264_mv.pmem_fd = pmem_fd;
  h264_mv.offset = 0;

  ioctl_msg.in = (void*)&h264_mv;
  ioctl_msg.out = NULL;

  if (ioctl (decode_context->video_driver_fd,VDEC_IOCTL_SET_H264_MV_BUFFER, (void*)&ioctl_msg) < 0)
  {
    DEBUG_PRINT("Failed to set the H264_mv_buffers\n");
    return -1;
  }

  h264_mv_buff.buffer = (unsigned char *) buf_addr;
  h264_mv_buff.size = size;
  h264_mv_buff.count = decode_context->output_buffer.actualcount;
  h264_mv_buff.offset = 0;
  h264_mv_buff.pmem_fd = pmem_fd;
  DEBUG_PRINT("Saving virt:%p, FD: %d of size %d count: %d", h264_mv_buff.buffer,
                   h264_mv_buff.pmem_fd, h264_mv_buff.size, decode_context->output_buffer.actualcount);
  return 1;
}

int allocate_reconfig_buff(void *context)
{
  struct vdec_ioctl_msg ioctl_msg = {NULL,NULL};
  struct video_decoder_context *decode_context = NULL;
  struct vdec_input_frameinfo frameinfo;
  struct vdec_fillbuffer_cmd fillbuffer;
  unsigned int i = 0;
  unsigned int data_len =0;

  memset ((unsigned char*)&frameinfo,0,sizeof (struct vdec_input_frameinfo));
  memset ((unsigned char*)&fillbuffer,0,sizeof (struct vdec_fillbuffer_cmd));

  decode_context = (struct video_decoder_context *) context;
  decode_context->output_buffer.buffer_type = VDEC_BUFFER_TYPE_OUTPUT;
  ioctl_msg.in = NULL;
  ioctl_msg.out = &decode_context->output_buffer;

  if (ioctl (decode_context->video_driver_fd,VDEC_IOCTL_GET_BUFFER_REQ,
         (void*)&ioctl_msg) < 0)
  {
    DEBUG_PRINT("\n 2nd time Requesting for output buffer requirements failed");
    return -1;
  }

  DEBUG_PRINT("\n 2nd time output_buffer Size=%d min count =%d actual count = %d, maxcount = %d,alignment = %d,buf_poolid= %d,meta_buffer_size=%d\n\n",\
              decode_context->output_buffer.buffer_size,\
              decode_context->output_buffer.mincount,\
              decode_context->output_buffer.actualcount,\
              decode_context->output_buffer.maxcount,\
              decode_context->output_buffer.alignment,\
              decode_context->output_buffer.buf_poolid,\
              decode_context->output_buffer.meta_buffer_size);
   vdec_alloc_h264_mv(decode_context);
   if ((ion_allocate_buffer(VDEC_BUFFER_TYPE_OUTPUT,
                      decode_context)== -1))
   {
     DEBUG_PRINT("\n Error in output Buffer allocation");
   }
  i = 0;
  while (i < decode_context->output_buffer.mincount)
  {
    fillbuffer.buffer.buffer_len =
                               decode_context->ptr_outputbuffer [i].buffer_len;
    fillbuffer.buffer.bufferaddr =
                               decode_context->ptr_outputbuffer [i].bufferaddr;
    fillbuffer.buffer.offset =
                               decode_context->ptr_outputbuffer [i].offset;
    fillbuffer.buffer.pmem_fd =
                               decode_context->ptr_outputbuffer [i].pmem_fd;
    fillbuffer.client_data = (void *)&decode_context->ptr_respbuffer[i];
    DEBUG_PRINT ("\n Client Data on output = %p and bufferaddr = %p",fillbuffer.client_data,decode_context->ptr_outputbuffer [i].bufferaddr);
    ioctl_msg.in = &fillbuffer;
    ioctl_msg.out = NULL;

    if (ioctl (decode_context->video_driver_fd,
           VDEC_IOCTL_FILL_OUTPUT_BUFFER,&ioctl_msg) < 0)
    {
      DEBUG_PRINT("\n Decoder frame failed");
      return -1;
    }
    i++;
  }


  return 1;
}
static void* video_thread (void *context)
{
   struct video_decoder_context *decode_context = NULL;
   struct video_msgq *queueitem = NULL;
   struct vdec_ioctl_msg ioctl_msg = {NULL,NULL};
   struct vdec_input_frameinfo frameinfo;
   struct vdec_fillbuffer_cmd fillbuffer;
   struct vdec_output_frameinfo *outputbuffer = NULL;
   struct vdec_bufferpayload *tempbuffer = NULL;
   unsigned int data_len =0;


   if (context == NULL)
   {
     DEBUG_PRINT("\n video thread recieved NULL context");
     return NULL;
   }
   decode_context = (struct video_decoder_context *) context;

   /* Thread function which will accept commands from async thread
    * or main thread
   */
   while (1)
   {
      queueitem = queue_get_cmd (&decode_context ->queue_context);
      if (queueitem != NULL)
      {
        switch (queueitem->cmd)
        {
        case VDEC_MSG_EVT_HW_ERROR:
          DEBUG_PRINT("\n FATAL ERROR ");
          break;
        case VDEC_MSG_RESP_INPUT_FLUSHED:
          break;
        case VDEC_MSG_RESP_OUTPUT_FLUSHED:
          break;
        case VDEC_MSG_RESP_START_DONE:
          DEBUG_PRINT("\n recived start done command");
            sem_post (&decode_context->sem_synchronize);
          break;

        case VDEC_MSG_RESP_STOP_DONE:
          DEBUG_PRINT("\n recieved stop done");
          sem_post (&decode_context->sem_synchronize);
          break;

        case VDEC_MSG_RESP_INPUT_BUFFER_DONE:

          tempbuffer = (struct vdec_bufferpayload *)queueitem->clientdata;
          if (tempbuffer == NULL)
          {
            DEBUG_PRINT("\n FATAL ERROR input buffer address is bad");
            sem_post (&decode_context->sem_synchronize);
            break;
          }
          DEBUG_PRINT("\n buffaddr in INPUT_BUF_DONE : %p",tempbuffer->bufferaddr);
          if(file_type == 1)
	  {
	     data_len = read_frame_from_container(tempbuffer->bufferaddr,\
                       tempbuffer->buffer_len,decode_context->demux_data);
	  }
          else if(file_type == 2)
	  {
           data_len = read_frame ( tempbuffer->bufferaddr,
                        tempbuffer->buffer_len,
                        decode_context->inputBufferFile
                     );

          }

          if (data_len == 0)
          {
            DEBUG_PRINT ("\n End of stream reached");
            frameinfo.flags |= VDEC_BUFFERFLAG_EOS;
  //          sem_post (&decode_context->sem_synchronize);
  //          break;
          }

          frameinfo.bufferaddr = tempbuffer->bufferaddr;
          frameinfo.offset = 0;
          frameinfo.pmem_fd = tempbuffer->pmem_fd;
          frameinfo.pmem_offset = tempbuffer->offset;
          frameinfo.datalen = data_len;
          frameinfo.client_data = (struct vdec_bufferpayload *)\
                       tempbuffer;
          /*TODO: Time stamp needs to be updated*/
          ioctl_msg.in = &frameinfo;
          ioctl_msg.out = NULL;
          total_frames++;
          if (ioctl(decode_context->video_driver_fd,VDEC_IOCTL_DECODE_FRAME,
               &ioctl_msg) < 0)
          {
            DEBUG_PRINT("\n Decoder frame failed");
            sem_post (&decode_context->sem_synchronize);
          }

          DEBUG_PRINT("\n Input buffer done send next buffer current value = %d",\
                      total_frames);
          break;

        case VDEC_MSG_EVT_CONFIG_CHANGED:
          reconfig =1;

          ioctl_msg.in = &decode_context->output_buffer;
          ioctl_msg.out = NULL;
          if (ioctl(decode_context->video_driver_fd, VDEC_IOCTL_CMD_FLUSH, &ioctl_msg) < 0)
          {
	    DEBUG_PRINT("\n >>>>>>>>>>> Flush Port  Failed ");
          }
          (void)free_buffer(VDEC_BUFFER_TYPE_OUTPUT,decode_context);
          break;

        case VDEC_MSG_RESP_OUTPUT_BUFFER_DONE:
         outputbuffer = (struct vdec_output_frameinfo *)\
                              queueitem->clientdata;
          if (outputbuffer == NULL || outputbuffer->bufferaddr == NULL ||
                   outputbuffer->client_data == NULL
           )
         {
           DEBUG_PRINT("\n FATAL ERROR output buffer is bad");
           DEBUG_PRINT("\nValues outputbuffer = %p",outputbuffer);
           if (outputbuffer != NULL)
           {
           DEBUG_PRINT("\nValues outputbuffer->bufferaddr = %p",\
                       outputbuffer->bufferaddr);
           DEBUG_PRINT("\nValues outputbuffer->client_data = %p",\
                       outputbuffer->client_data);
           }
           sem_post (&decode_context->sem_synchronize);
           break;
         }


         if (outputbuffer->len == 0)
         {
           DEBUG_PRINT("\n Filled Length is zero Close decoding");
           sem_post (&decode_context->sem_synchronize);
           break;
         }

         if (decode_context->outputBufferFile != NULL)
         {
           fwrite (outputbuffer->bufferaddr,1,outputbuffer->len,
                decode_context->outputBufferFile);
         }

         tempbuffer = (struct vdec_bufferpayload *)\
                     outputbuffer->client_data;

         fillbuffer.buffer.buffer_len = tempbuffer->buffer_len;
         fillbuffer.buffer.bufferaddr = tempbuffer->bufferaddr;
         fillbuffer.buffer.offset = tempbuffer->offset;
         fillbuffer.buffer.pmem_fd = tempbuffer->pmem_fd;
         fillbuffer.client_data = (void *)outputbuffer;

         ioctl_msg.in = &fillbuffer;
         ioctl_msg.out = NULL;

         if (ioctl (decode_context->video_driver_fd,
              VDEC_IOCTL_FILL_OUTPUT_BUFFER,&ioctl_msg) < 0)
         {
           DEBUG_PRINT("\n Decoder frame failed");
           return NULL;
         }

         break;

        case VDEC_MSG_RESP_FLUSH_INPUT_DONE:
            DEBUG_PRINT("\n Flush input complete");
          sem_post (&decode_context->sem_synchronize);
          break;

        case VDEC_MSG_RESP_FLUSH_OUTPUT_DONE:
          DEBUG_PRINT("\n ---------- Flush output complete -----------------");
          if(reconfig == 1)
          {
            allocate_reconfig_buff(decode_context);
            DEBUG_PRINT("\n ---------- reconfig set to 0 -----------------");
            reconfig = 0;
          }
          else
               sem_post (&decode_context->sem_synchronize);
          break;
        default :
          DEBUG_PRINT("\n Video thread default case:: \n");
        }

        if (queueitem->cmd == VDEC_MSG_RESP_STOP_DONE)
        {
          DEBUG_PRINT("\n Playback has ended thread will exit");
          return NULL;
        }
      }
      else
      {
        DEBUG_PRINT("\n Error condition recieved NULL from Queue");
      }

   }
}

static void* async_thread (void *context)
{
  struct video_decoder_context *decode_context = NULL;
  struct vdec_output_frameinfo *outputframe = NULL;
  struct video_msgq queueitem ;
  struct vdec_msginfo vdec_msg;
  struct vdec_ioctl_msg ioctl_msg = {NULL,NULL};
  int result = -1;

  if (context == NULL)
  {
    DEBUG_PRINT("\n aynsc thread recieved NULL context");
    return NULL;
  }
  decode_context = (struct video_decoder_context *) context;
  DEBUG_PRINT("\n Entering the async thread");

  while (1)
  {
    ioctl_msg.in = NULL;

    ioctl_msg.out = (void*)&vdec_msg;
    if (ioctl (decode_context->video_driver_fd,VDEC_IOCTL_GET_NEXT_MSG,\
         (void*)&ioctl_msg) < 0)
    {
      DEBUG_PRINT("\n Error in ioctl read next msg");
    }
    else
    {
      DEBUG_PRINT("\n\nioctl read next msg  code : %d",vdec_msg.msgcode);
      switch (vdec_msg.msgcode)
      {
      case VDEC_MSG_RESP_FLUSH_INPUT_DONE:
      case VDEC_MSG_RESP_FLUSH_OUTPUT_DONE:
      case VDEC_MSG_RESP_START_DONE:
      case VDEC_MSG_RESP_STOP_DONE:
      case VDEC_MSG_EVT_HW_ERROR:
        queueitem.cmd = vdec_msg.msgcode;
        queueitem.status = vdec_msg.status_code;
        queueitem.clientdata = NULL;
        break;

      case VDEC_MSG_RESP_INPUT_FLUSHED:
      case VDEC_MSG_RESP_INPUT_BUFFER_DONE:

        queueitem.cmd = vdec_msg.msgcode;
        queueitem.status = vdec_msg.status_code;
        queueitem.clientdata = (void *)\
            vdec_msg.msgdata.input_frame_clientdata;
        break;

      case VDEC_MSG_RESP_OUTPUT_FLUSHED:
      case VDEC_MSG_RESP_OUTPUT_BUFFER_DONE:
        queueitem.cmd = vdec_msg.msgcode;
        queueitem.status = vdec_msg.status_code;
        outputframe = (struct vdec_output_frameinfo *)\
        vdec_msg.msgdata.output_frame.client_data;
        outputframe->bufferaddr = vdec_msg.msgdata.output_frame.bufferaddr;
        outputframe->framesize.bottom = \
        vdec_msg.msgdata.output_frame.framesize.bottom;
        outputframe->framesize.left = \
        vdec_msg.msgdata.output_frame.framesize.left;
        outputframe->framesize.right = \
        vdec_msg.msgdata.output_frame.framesize.right;
        outputframe->framesize.top = \
        vdec_msg.msgdata.output_frame.framesize.top;
        outputframe->framesize = vdec_msg.msgdata.output_frame.framesize;
        outputframe->len = vdec_msg.msgdata.output_frame.len;
        outputframe->time_stamp = vdec_msg.msgdata.output_frame.time_stamp;
        queueitem.clientdata = (void *)outputframe;
        break;
      default:
        DEBUG_PRINT("\nIn Default of get next message %d",vdec_msg.msgcode);
        queueitem.cmd = vdec_msg.msgcode;
        queueitem.status = vdec_msg.status_code;
        queueitem.clientdata = NULL;
        break;
      }
      result = queue_post_cmdq (&decode_context->queue_context,&queueitem);
      while (result == 0)
      {
         result = queue_post_cmdq (&decode_context->queue_context,
                 &queueitem);
      }

      if (result == -1)
      {
        DEBUG_PRINT("\n FATAL ERROR WITH Queue");
      }
    }
    if (vdec_msg.msgcode == VDEC_MSG_RESP_STOP_DONE)
    {
      /*Thread can exit at this point*/
      return NULL;
    }
  }
}

static unsigned int read_frame_from_container(unsigned char *dataptr, unsigned int length,struct demux *demux_ptr)
{
  AVPacket pk = {};
  int size = demux_ptr->cc->width * demux_ptr->cc->height;

  while (!av_read_frame(demux_ptr->afc, &pk)) {
      if (pk.stream_index == demux_ptr->st->index) {
      uint8_t *buf;
      int bufsize;
      if (demux_ptr->bsf) {
        int ret;
        ret = av_bitstream_filter_filter(demux_ptr->bsf, demux_ptr->cc,
					NULL, &buf, &bufsize, pk.data, pk.size, 0);
        if (ret < 0) {
            printf("bsf error: %d", ret);
            return 0;
          }
        } else {
          buf = pk.data;
          bufsize = pk.size;
        }
        if (bufsize > size)
        {
            bufsize = size;
            printf("\nprasanth BUFSIZE :%d , size:%d",bufsize,size);
        }
        memcpy(dataptr, buf, bufsize);
        if (demux_ptr->bsf)
            av_free(buf);
        av_free_packet(&pk);
        return bufsize;
      }
      av_free_packet(&pk);
    }
  return 0;
}
static unsigned int read_frame (unsigned char *dataptr, unsigned int length,
                                FILE * inputBufferFile)
{

  unsigned int readOffset = 0;
  int bytes_read = 0;
  unsigned int code = 0;
  int found = 0;

  if (dataptr == NULL || length == 0)
  {
    DEBUG_PRINT ("\n dataptr = %p length = %u",dataptr,length);
    return 0;
  }

  if (Codec_type == VDEC_CODECTYPE_MPEG4)
  {
    /* Start of Critical Section*/
    pthread_mutex_lock(&read_lock);
    do
    {
      //Start codes are always byte aligned.
      bytes_read = fread(&dataptr[readOffset],1, 1,inputBufferFile);
      if( !bytes_read)
      {
        DEBUG_PRINT("\n Bytes read Zero \n");
        break;
      }
      code <<= 8;
      code |= (0x000000FF & dataptr[readOffset]);
      //VOP start code comparision
      if (readOffset>3)
      {
        if(!header_code )
        {
          if( VOP_START_CODE == code)
          {
          DEBUG_PRINT ("\n Found VOP Code");
          header_code = VOP_START_CODE;
          }
          else if ( (0xFFFFFC00 & code) == SHORT_HEADER_START_CODE )
          {
          header_code = SHORT_HEADER_START_CODE;
          }
        }
        if ((header_code == VOP_START_CODE) && (code == VOP_START_CODE))
        {
          //Seek backwards by 4
          fseek(inputBufferFile, -4, SEEK_CUR);
          readOffset-=4;
          found = 1;
          break;

        }
        else if (( header_code == SHORT_HEADER_START_CODE ) &&
        ( SHORT_HEADER_START_CODE == (code & 0xFFFFFC00)))
        {
          //Seek backwards by 4
          fseek(inputBufferFile, -4, SEEK_CUR);
          readOffset-=4;
          found = 1;
          break;
        }
      }
      readOffset++;
    }while (readOffset < length);
    pthread_mutex_unlock(&read_lock);
    /* End of Critical Section*/
    if (found == 1)
    {
      //DEBUG_PRINT ("Found a Frame");
      return (readOffset+1);
    }
    else
    {
      //DEBUG_PRINT ("No Frames detected");
      return 0;
    }
  }
  else if (Codec_type == VDEC_CODECTYPE_H264)
  {
    /* Start of Critical Section*/
    pthread_mutex_lock(&read_lock);
    do
    {
      //Start codes are always byte aligned.
      bytes_read = fread(&dataptr[readOffset],1, 1,inputBufferFile);
      if( !bytes_read)
      {
        DEBUG_PRINT("\n Bytes read Zero \n");
        break;
      }
      code <<= 8;
      code |= (0x000000FF & dataptr[readOffset]);

      //VOP start code comparision
      if (readOffset>3)
      {
        if(!header_code )
        {
          if( H264_START_CODE == code)
          {
          header_code = H264_START_CODE;
          }
          else if ( (0xFFFFFC00 & code) == SHORT_HEADER_START_CODE )
          {
           DEBUG_PRINT ("\n prasanth Found SHORT_HEADER_START_CODE");
          header_code = SHORT_HEADER_START_CODE;
          }
        }
        //DEBUG_PRINT ("\n prasanth code : 0x%x",code);
        if ((header_code == H264_START_CODE) && (code == H264_START_CODE))
        {
          //Seek backwards by 4
          fseek(inputBufferFile, -4, SEEK_CUR);
          readOffset-=4;
          found = 1;
           DEBUG_PRINT ("\n ---------- prasanth code detected : 0x%x",code);
          break;

        }
     /*   else if (( header_code == SHORT_HEADER_START_CODE ) &&
        ( SHORT_HEADER_START_CODE == (code & 0xFFFFFC00)))
        {
          //Seek backwards by 4
          fseek(inputBufferFile, -4, SEEK_CUR);
          readOffset-=4;
          found = 1;
          break;
        }*/

      }
      readOffset++;
    }while (readOffset < length);
    pthread_mutex_unlock(&read_lock);
    /* End of Critical Section*/
    if (found == 1)
    {
      DEBUG_PRINT ("\nread_frame Found a Frame adn length : %d, code : 0x%x",readOffset,code);
      return (readOffset+1);
    }
    else
    {
      DEBUG_PRINT ("No Frames detected");
      return 0;
    }
  }
  else
  {
    DEBUG_PRINT ("\n else part of the Inside the readframe Codec_type : %d",Codec_type);
    readOffset = Read_Buffer_From_DAT_File(dataptr,length,inputBufferFile);
    if (total_frames == 0)
    {
      bytes_read = Read_Buffer_From_DAT_File(&dataptr[readOffset],
                                             (length-readOffset),
                                             inputBufferFile);
      readOffset += bytes_read;
    }
    return (readOffset);
  }

}

static int Read_Buffer_From_DAT_File(unsigned char *dataptr, unsigned int length,
                                     FILE * inputBufferFile)
{

  long frameSize=0;
  char temp_buffer[10];
  char temp_byte;
  int bytes_read=0;
  int i=0;
  unsigned char *read_buffer=NULL;
  char c = '1'; //initialize to anything except '\0'(0)
  char inputFrameSize[12];
  int count =0; char cnt =0;
  memset(temp_buffer, 0, sizeof(temp_buffer));

  while (cnt < 10)
  /* Check the input file format, may result in infinite loop */
  {
      count  = fread(&inputFrameSize[cnt],1,1,inputBufferFile);
      if(inputFrameSize[cnt] == '\0' )
        break;
      cnt++;
  }
  inputFrameSize[cnt]='\0';
  frameSize = atoi(inputFrameSize);
  //length = 0;
  DEBUG_PRINT ("\n Frame Size is %ld",frameSize);

  /* get the frame length */
  fseek(inputBufferFile, -1, SEEK_CUR);
  bytes_read = fread(dataptr, 1, frameSize,  inputBufferFile);

  if(bytes_read == 0 || bytes_read < frameSize ) {
      return 0;
  }
  return bytes_read;
}
