/**************************************************************************
 *
 * Copyright © 2009 VMware, Inc., Palo Alto, CA., USA
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sub license, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDERS, AUTHORS AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/


#ifndef _LIBMSM_H_
#define _LIBMSM_H_

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

/**
 * \file
 *
 */

struct msm_driver;
struct msm_bo;

enum msm_attrib
{
	MSM_TERMINATE_PROP_LIST,
#define MSM_TERMINATE_PROP_LIST MSM_TERMINATE_PROP_LIST
	MSM_BO_TYPE,
#define MSM_BO_TYPE MSM_BO_TYPE
	MSM_WIDTH,
#define MSM_WIDTH MSM_WIDTH
	MSM_HEIGHT,
#define MSM_HEIGHT MSM_HEIGHT
	MSM_PITCH,
#define MSM_PITCH MSM_PITCH
	MSM_HANDLE,
#define MSM_HANDLE MSM_HANDLE
};

enum msm_bo_type
{
	MSM_BO_TYPE_SCANOUT_X8R8G8B8 = (1 << 0),
#define MSM_BO_TYPE_SCANOUT_X8R8G8B8 MSM_BO_TYPE_SCANOUT_X8R8G8B8
	MSM_BO_TYPE_CURSOR_64X64_A8R8G8B8 =  (1 << 1),
#define MSM_BO_TYPE_CURSOR_64X64_A8R8G8B8 MSM_BO_TYPE_CURSOR_64X64_A8R8G8B8
};

int msm_create(int fd, struct msm_driver **out);
int msm_get_prop(struct msm_driver *msm, unsigned key, unsigned *out);
int msm_destroy(struct msm_driver **msm);

int msm_bo_create(struct msm_driver *msm, const unsigned *attr, struct msm_bo **out, int buf_fd);
int msm_bo_get_prop(struct msm_bo *bo, unsigned key, unsigned *out);
int msm_bo_map(struct msm_bo *bo, void **out);
int msm_bo_unmap(struct msm_bo *bo);
int msm_bo_destroy(struct msm_bo **bo);

#if defined(__cplusplus) || defined(c_plusplus)
};
#endif

#endif
