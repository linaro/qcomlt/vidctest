/**************************************************************************
 *
 * Copyright © 2009 VMware, Inc., Palo Alto, CA., USA
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sub license, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDERS, AUTHORS AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/


#define HAVE_STDINT_H
#define _FILE_OFFSET_BITS 64

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "msmdumb_priv.h"

#include <sys/mman.h>
#include <sys/ioctl.h>
#include "xf86drm.h"

#include "i915_drm.h"

struct dumb_bo
{
	struct msm_bo base;
	unsigned map_count;
};

static int
dumb_get_prop(struct msm_driver *msm, unsigned key, unsigned *out)
{
	switch (key) {
	case MSM_BO_TYPE:
		*out = MSM_BO_TYPE_SCANOUT_X8R8G8B8 | MSM_BO_TYPE_CURSOR_64X64_A8R8G8B8;
		break;
	default:
		return -EINVAL;
	}
	return 0;
}

static int
dumb_destroy(struct msm_driver *msm)
{
	free(msm);
	return 0;
}

static int
dumb_bo_create(struct msm_driver *msm,
		 const unsigned width, const unsigned height,
		 const enum msm_bo_type type, const unsigned *attr,
		 struct msm_bo **out, int buf_fd)
{
	struct drm_mode_create_dumb arg;
	struct dumb_bo *bo;
	int i, ret;
   uint32_t prime_handle = 0;

	for (i = 0; attr[i]; i += 2) {
		switch (attr[i]) {
		case MSM_WIDTH:
		case MSM_HEIGHT:
			break;
		case MSM_BO_TYPE:
			break;
		default:
			return -EINVAL;
		}
	}

	bo = calloc(1, sizeof(*bo));
	if (!bo)
		return -ENOMEM;

	memset(&arg, 0, sizeof(arg));

	/* All BO_TYPE currently are 32bpp formats */
	arg.bpp = 32;
	arg.width = width;
	arg.height = height;

   ret = drmPrimeFDToHandle(msm->fd, (uint32_t) buf_fd, &prime_handle);

   if (ret) {
      fprintf(stderr, "\n drmPrimeHandleToFD failed with error %d ", ret);
      goto err_free;
   }

   bo->base.handle = (unsigned) prime_handle;
   bo->base.msm = msm;
#define ALIGN(v,a) (((v) + (a) - 1) & ~((a) - 1))
   bo->base.pitch = ((arg.bpp + 7)/8) * (ALIGN(width, 32));
   bo->base.size  = ALIGN(bo->base.pitch * height, 0x1000);

	*out = &bo->base;

	return 0;

err_free:
	free(bo);
	return ret;
}

static int
dumb_bo_get_prop(struct msm_bo *bo, unsigned key, unsigned *out)
{
	switch (key) {
	default:
		return -EINVAL;
	}
}

static int
dumb_bo_map(struct msm_bo *_bo, void **out)
{
	struct dumb_bo *bo = (struct dumb_bo *)_bo;
	struct drm_mode_map_dumb arg;
	void *map = NULL;
	int ret;

	if (bo->base.ptr) {
		bo->map_count++;
		*out = bo->base.ptr;
		return 0;
	}

	memset(&arg, 0, sizeof(arg));
	arg.handle = bo->base.handle;

	ret = drmIoctl(bo->base.msm->fd, DRM_IOCTL_MODE_MAP_DUMB, &arg);
	if (ret)
		return ret;

	map = mmap(0, bo->base.size, PROT_READ | PROT_WRITE, MAP_SHARED, bo->base.msm->fd, arg.offset);
	if (map == MAP_FAILED)
		return -errno;

	bo->base.ptr = map;
	bo->map_count++;
	*out = bo->base.ptr;

	return 0;
}

static int
dumb_bo_unmap(struct msm_bo *_bo)
{
	struct dumb_bo *bo = (struct dumb_bo *)_bo;
	bo->map_count--;
	return 0;
}

static int
dumb_bo_destroy(struct msm_bo *_bo)
{
	struct dumb_bo *bo = (struct dumb_bo *)_bo;

	if (bo->base.ptr) {
		/* XXX Sanity check map_count */
		munmap(bo->base.ptr, bo->base.size);
		bo->base.ptr = NULL;
	}

	free(bo);
	return 0;
}

int
msmdumb_create(int fd, struct msm_driver **out)
{
	struct msm_driver *msm;
	int ret;
	uint64_t cap = 0;

	ret = drmGetCap(fd, DRM_CAP_DUMB_BUFFER, &cap);
	if (ret || cap == 0)
		return -EINVAL;

	msm = calloc(1, sizeof(*msm));
	if (!msm)
		return -ENOMEM;

	msm->fd = fd;

	msm->bo_create = dumb_bo_create;
	msm->bo_map = dumb_bo_map;
	msm->bo_unmap = dumb_bo_unmap;
	msm->bo_get_prop = dumb_bo_get_prop;
	msm->bo_destroy = dumb_bo_destroy;
	msm->get_prop = dumb_get_prop;
	msm->destroy = dumb_destroy;
	*out = msm;

	return 0;
}
