/**************************************************************************
 *
 * Copyright © 2009 VMware, Inc., Palo Alto, CA., USA
 * All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sub license, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * THE COPYRIGHT HOLDERS, AUTHORS AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **************************************************************************/


//#include "config.h"
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "msmdumb_priv.h"

int msm_create(int fd, struct msm_driver **out)
{
	return msmdumb_create(fd, out);
}

int msm_get_prop(struct msm_driver *msm, unsigned key, unsigned *out)
{
	switch (key) {
	case MSM_BO_TYPE:
		break;
	default:
		return -EINVAL;
	}
	return msm->get_prop(msm, key, out);
}

int msm_destroy(struct msm_driver **msm)
{
	if (!(*msm))
		return 0;

	free(*msm);
	*msm = NULL;
	return 0;
}

int msm_bo_create(struct msm_driver *msm, const unsigned *attr, struct msm_bo **out, int buf_fd)
{
	unsigned width = 0;
	unsigned height = 0;
	enum msm_bo_type type = MSM_BO_TYPE_SCANOUT_X8R8G8B8;
	int i;

	for (i = 0; attr[i];) {
		unsigned key = attr[i++];
		unsigned value = attr[i++];

		switch (key) {
		case MSM_WIDTH:
			width = value;
			break;
		case MSM_HEIGHT:
			height = value;
			break;
		case MSM_BO_TYPE:
			type = value;
			break;
		default:
			return -EINVAL;
		}
	}

	if (width == 0 || height == 0)
		return -EINVAL;

	/* XXX sanity check type */

	if (type == MSM_BO_TYPE_CURSOR_64X64_A8R8G8B8 &&
	    (width != 64 || height != 64))
		return -EINVAL;

	return msm->bo_create(msm, width, height, type, attr, out, buf_fd);
}

int msm_bo_get_prop(struct msm_bo *bo, unsigned key, unsigned *out)
{
	switch (key) {
	case MSM_PITCH:
		*out = bo->pitch;
		break;
	case MSM_HANDLE:
		*out = bo->handle;
		break;
	default:
		return -EINVAL;
	}

	return 0;
}

int msm_bo_map(struct msm_bo *bo, void **out)
{
	return bo->msm->bo_map(bo, out);
}

int msm_bo_unmap(struct msm_bo *bo)
{
	return bo->msm->bo_unmap(bo);
}

int msm_bo_destroy(struct msm_bo **bo)
{
	int ret;

	if (!(*bo))
		return 0;

	ret = (*bo)->msm->bo_destroy(*bo);
	if (ret)
		return ret;

	*bo = NULL;
	return 0;
}
